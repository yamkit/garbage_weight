require "serialport"
require "net/http"


while true
  # read serial port data
  ser = SerialPort.new(2, 9600, 8, 1, SerialPort::NONE)
  ser.read_timeout = 2000

  ser.write("Yy")
  puts "==data is reading=="

  begin
    weight = ser.read.to_i

    if weight > 100
      puts "weight is #{weight}"
      uri = URI("http://61.139.87.61:9292/weight_records/upload_weight")
      res = Net::HTTP.post_form(uri, 'weight' => weight)
    end
  rescue Exception => e
    File.open("client_error.log", "w") {|file| file.puts("#{DateTime.now}: #{e}")}
  end

  ser.close
end
